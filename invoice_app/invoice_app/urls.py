from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from registration.backends.default.views import RegistrationView

urlpatterns = [
    path('admin/', admin.site.urls),

    path('app/accounts/', include('registration.backends.default.urls')),

    path('api-auth/', include('rest_framework.urls')),

    path('api/v1/invoice/', include(('api.v1.invoice.urls', 'v1_invoice'))),

    path('', include(("invoice.urls", 'invoice'))),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
