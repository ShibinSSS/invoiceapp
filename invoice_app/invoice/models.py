import uuid
from django.db import models


class Invoice(models.Model):
    invoice_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    client_name = models.CharField(max_length=250, unique=True)
    client_email = models.EmailField(max_length=250, unique=True)
    project_name = models.CharField(max_length=250)
    amount = models.IntegerField()
    is_paid = models.BooleanField(default=False)

    class Meta:
        ordering = ('amount',)
        verbose_name = 'Invoice'
        verbose_name_plural = 'Invoices'

    def __str__(self):
        return self.client_name
