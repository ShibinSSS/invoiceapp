from django.urls import path,re_path

from invoice import views

urlpatterns = [
    path('', views.invoices, name="home"),
    path('create_invoice', views.create_invoice, name="create_invoice"),
    path('make-payment/<pk>', views.make_payment, name='make_payment'),
]