from django.shortcuts import render,reverse
from django.http import HttpResponse, HttpResponseRedirect
from invoice.services import get_books
import requests
from django.conf import settings
from django.contrib.auth.decorators import login_required

@login_required
def create_invoice(request):

    # check the request and save using the requests package through REST end
   if request.method == 'POST':
        name = request.POST.get('client_name')
        email = request.POST.get('client_email')
        project = request.POST.get('project_name')
        amount = request.POST.get('amount')

        data = {
            'client_name': name,
            'client_email': email,
            'project_name': project,
            'amount':amount,
        }
        response = requests.post('http://127.0.0.1:8000/api/v1/invoice/create-invoice/', data=data)
        print(response)
        return HttpResponseRedirect('/')

   context = {
       "is_home":True,
   }
   return render(request,'invoice/add-invoice.html',context)

@login_required
def invoices(request):
    # fetch all the values in a database using REST
    response = requests.get('http://127.0.0.1:8000/api/v1/invoice/invoices')

    invoices = response.json()
    context = {
        "invoices":invoices['data'],
        "is_invoices":True,
    }
    return render(request,'invoice/invoices.html',context)


def make_payment(request,pk):
    # The window for making the customer payment the url
    response = requests.get('http://127.0.0.1:8000/api/v1/invoice/payment/'+pk)

    invoice = response.json()
    key = settings.STRIPE_PUBLISHABLE_KEY
    context = {
        "invoice": invoice['data'],
        "key":key,
        "is_pay": True,
    }

    if request.method == 'POST':
        data = {
            'is_paid':True
        }
        response = requests.put('http://127.0.0.1:8000/api/v1/invoice/payment-sucess/'+pk+'/', data=data)
        return HttpResponse("Sucess")

    return render(request, 'invoice/view-invoice.html', context)