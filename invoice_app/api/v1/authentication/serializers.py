from django.db.models import fields
from rest_framework import serializers
from lessons.models import UploadLesson


class LessonSerializer(serializers.ModelSerializer):

    class Meta:
        model = UploadLesson
        fields = '__all__'

class LessonEditSerializer(serializers.ModelSerializer):

    # image = serializers.ImageField('image', required=False)

    class Meta:
        model = UploadLesson
        fields = ['content','image','video','audio']