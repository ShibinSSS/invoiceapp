# normal urls.py
from django.conf.urls import url,include
from . import views


urlpatterns = [
   url(r'^create-invoice/$', views.create_invoice, name='create_invoice'),
   url(r'^invoices/$', views.invoices, name='invoices'),
   url(r'^payment/(?P<pk>.*)/$', views.invoice_payment, name='invoice'),
   url(r'^payment-sucess/(?P<pk>.*)/$', views.payment_sucess, name='payment_sucess'),
   ]