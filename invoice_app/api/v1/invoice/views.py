from django.shortcuts import (get_object_or_404, )
from rest_framework import status
from rest_framework.decorators import (api_view, permission_classes, renderer_classes)
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from api.v1.invoice.general.functions import generate_serialzer_errors
from api.v1.invoice.serializers import (InvoiceSerializer,InvoiceEditSerializer)
from invoice.models import Invoice


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def create_invoice(request):
    serialized = InvoiceSerializer(data=request.data)
    if serialized.is_valid():
        serialized.save()

        response_data = {
            "StatusCode": 6000,
            "data": serialized.data
        }

        return Response(response_data, status=status.HTTP_200_OK)

    else:
        response_data = {
            "StatusCode": 6001,
            "message": generate_serialzer_errors(serialized._errors)
        }
        return Response(response_data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def invoices(request):
    instances = Invoice.objects.all()
    serialized = InvoiceSerializer(instances, many=True, context={"request": request})

    response_data = {
        "data": serialized.data
    }

    return Response(response_data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def invoice_payment(request, pk):
    instances = None
    if Invoice.objects.filter(invoice_id=pk).exists():
        instances = Invoice.objects.get(invoice_id=pk)

    if instances:
        serialized = InvoiceSerializer(instances, context={"request": request})
        response_data = {
            "data": serialized.data,
        }

        return Response(response_data, status=status.HTTP_200_OK)

    else:
        response_data = {
            "StatusCode": 6001,
            "data": "Not found"
        }

        return Response(response_data, status=status.HTTP_200_OK)


@api_view(['PUT'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def payment_sucess(request, pk):
    serialzed = InvoiceEditSerializer(data=request.data)
    print(serialzed)

    instances = None
    if Invoice.objects.filter(pk=pk).exists():
        instances = Invoice.objects.get(pk=pk)

    if instances:
        if serialzed.is_valid():
            serialzed.update(instances, serialzed.data)
            response_data = {
                "data": serialzed.data
            }
            return Response(response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                "StatusCode": 6001,
                "message": generate_serialzer_errors(serialzed.errors)
            }
        return Response(response_data, status=status.HTTP_200_OK)

    else:
        response_data = {
            "StatusCode": 6001,
            "data": "Not found"
        }

        return Response(response_data, status=status.HTTP_200_OK)