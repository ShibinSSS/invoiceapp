from rest_framework import serializers
from invoice.models import Invoice
# from django.contrib.auth.models import get_user_model


class InvoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Invoice
        fields = '__all__'

class InvoiceEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Invoice
        fields = ['is_paid']